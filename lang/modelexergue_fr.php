<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'modelexergue_titre' => 'Modèle exergue',
	// T
	'modelexergue_texteinserer' => 'Texte à mettre en exergue',
	// U
	'modelexergue_inserer' => 'un exergue',
);
